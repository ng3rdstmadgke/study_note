==============================
イントロダクション
==============================

インストール
=============
sudo apt-get install git

バージョン確認
=============
git --version

初期設定
=============
git config --global user.name “ng3rdstmadgke”
git config --global user.email “ng3rdstmadgke@gmail.com”
git config --global color.ui auto

ローカルリポジトリの作成
==========================
git init


=============================
コマンド
=============================
- add
====================
git add [filename]
git add .	#変更・追加したファイルすべて
git add -u	#変更したファイルのみ
git reset HEAD -- [file_PATH]		#インデックスに登録したファイルを取り消す


- commit
====================
git commit -m “coment”
git commit
git commit --amend		# 直前のコミット内容を編集する
例)
git add .
git commit --amend		# 新しいファイルを直前のコミットに追加できる


- diff
====================
差分を見る
git diff  <old_commit> <new_commit> <file_PATH>
git diff --cached	#インデックスとリポジトリの差分
git diff HEAD		#ワーキングディレクトリとリポジトリの差分


- status
====================
変更ファイルの状態を見る
git status


- log
====================
git log <file_PATH>	#file_PATHをつけると特定のファイルのコミットだけを表示する


- checkout
====================
git checkout [branch_name]		# brance_nameにブランチを切り替えるする。
git checkout HEAD .		#直前のコミットをワーキングディレクトリに反映
git checkout [commit_hash] <file_PATH>	#<file_PATH>をつけて特定のfileのみコミットの状態に戻す
git checkout -b [new_branch_name]		#detached状態からブランチを切る
例）前のコミットに戻って新しいブランチを作る
git checkout ewrctvybuni9um0u98574675t89n8mjun
git checkout -b new_branch


- reset
====================
git reset --hard [commit id]	# ワークツリーを含めたすべてをコミットIDの状態に戻す(新たなブランチはできない)
git reset HEAD -- [filename]			# インデックスからファイルをアンステージする(新たなブランチはできない)
例)
git reset HEAD .		# すべてのファイルをアンステージする


- branch
====================
git branch				#現在のブランチを確認
git branch [new branch name]	#新しいブランチを作製
git branch -d [branch name]		#ブランチを削除


- fetch
====================
git fetch [remote_repo] [remote_branch]
例)
git fetch origin もしくは git fetch origin master
git fetch git@bitbucket.org:ng3rdstmadgke/study_note.git
※fetchを行うと「remotes/origin/master」のような新しいブランチができるので、本流にマージする
git merge remotes/origin/master


- merge　※本流のブランチで行う
====================
git merge [branch_name]


- clone
====================
リモートからクローン
git clone https://github.com/ng3rdstmadgke/my_py1.git
git clone ssh://midorikawakeita@210.140.76.224//opt/gitrepo/thoth.git


- remote
====================
リモートのハンドルを確認
git remote -v

リモートリポジトリの追加
git remote add [remote_name] [url]		#git remote add origin git://github.com/paulboone/ticgit.git
例）
git remote add origin git@bitbucket.org:ng3rdstmadgke/study_note.git

リモートへのプッシュ：githubにリポジトリを作成
git push [remote-name] [branch-name]	#git push origin master

リモートの調査
git remote show [remote_name]	#git remote show origin

リモートのリネーム
git remote rename [remote_name] [new_remote_name]	#git remote rename origin midori

リモートの削除
git remote rm origin



========================================
git & GitHub Tips集
========================================

GitHubでプロジェクトをフォーク
========================================
リモートリポジトリのブランチのようなもの。
開発の際は自分用のフォークを作り、変更が完了したら、フォーク元にプルリクエストを送り、マージしてもらう。


.gitignore
========================================
gitの中にビルドしたファイルを含ませたくない時は
https://github.com/github/gitignore/blob/master/Python.gitignoreのをコピペして作った
.gitignoreを.gitと同じディレクトリに配置する。


ローカルのリポジトリをgithubに登録するまで
========================================
1.ローカルにリポジトリを作る
git init
# README.mdと.gitignoreを作る
git add .
git commit -m "first commit"

2.githubにsshkeyを登録
右上のサムネイル >> settings >> 左側のSSH Keys >> sshkeyを登録titleは適当でOK

3.githubに新しくリポジトリを作成
	New repositoriesから新しくリポジトリを作成。sshをコピー

4.ローカルからリモートにadd & push
	git remote add origin git@github.com:ng3rdstmadgke/jQueryStartUp.git
	git push -u origin master


タグ付け
========================================
1.コミットハッシュに任意のタグをつける。
git tag release-0.1.0


リポジトリからファイルを削除する
========================================
1.gitリポジトリと元ファイルを削除
	git rm [file_name]

2.gitリポジトリと元ディレクトリを再帰的に削除
	git rm -r [dir_name]

3.gitのリポジトリからのみファイルを削除
	git rm --cached [file_name]

4.gitのリポジトリからのみディレクトリを再帰的に削除
	git rm --cached -r [dir_name]

５．git管理下でファイル名を変更する
	git mv [old_file_name] [new_file_name]


バックアップを作る
========================================
1.バックアップ用のリポジトリを作成する
$ touch back.git	#必ず拡張子を.gitにする
$ git --bare init		#back.git内で実行する

2.変更の履歴を送る
$ git push [送信先ディレクトリ] [送信元ブランチ]:[送信先ブランチ]
例)git push ~/back.git master:master	#バックアップを作成したいリポジトリがあるディレクトリでコマンドを実行
$ git clone [複製元リポジトリ]		#カレントディレクトリにバックアップを復元する
例)git clone ~/back.git


コンフリクトでpushできなくなってしまった場合
========================================
1. backアップとして新しいブランチを切る
git branch back_up
git branch -a    # 新しいブランチが作られたか確認

2. リモートから最新のコミットをfetch & merge
git fetch origin master
git merge /remote/origin/master    #masterブランチで行うこと
>> conflictが起こってmergeが失敗するはず

3. コンフリクトを解消する
git status    # conflictが起こっていれば「both modified:linux/nmcli.txt」と表示される
ファイルを直接編集しコンフリクトを解消
<<<<<<< HEAD
print("hello hello hello")	#HEAD：マージ元のみに存在する行
=======
print(datetime.date.today())	#newbr：マージ先のみに存在する行（マージしたい部分）
print("Hello Git")
for i in range(5):
    print(i, end="")
print("",end="\n\n")
>>>>>>> newbr
git add .     # git statusで上のメッセージが出なくなればOK

4. コミット
git commit -m "commit message"

5. プッシュ
git push origin master

6. バックアップしたブランチを削除
git branch -d back_up


指定の鍵でリモートリポジトリを登録したい時
================================================
sshのconfigにng3rdstmadgke.bitbucket.orgにsshした時は指定の鍵を使うよう設定
```.ssh/config
Host ng3rdstmadgke.bitbucket.org
  HostName bitbucket.org
  IdentityFile ~/.ssh/private_id_rsa
```
リポジトリのリモートリポジトリにng3rdstmadgke.bitbucket.orgを登録する
```
git remote add origin git@ng3rdstmadgke.bitbucket.org:ng3rdstmadgke/study_note.git
もしくは、すでにoriginが登録されている場合は
git remote set-url origin git@ng3rdstmadgke.bitbucket.org:ng3rdstmadgke/study_note.git
```
