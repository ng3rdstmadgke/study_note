static/google_analytics.js にトラッキングコードをコピペ

$ cd {Tinkerer のルートディレクトリ}
$ mkdir _static
$ touch _static/google_analytics.js
テンプレートを作り
$ touch _templates/page.html
テンプレートからトラッキングコードを読み込んで
_templates/page.html を編集

{% extends "!page.html" %}

{% set script_files = script_files + ["_static/google_analytics.js"] %}
ビルドしておわり
$ tinker -b
