=======================================
devstack実行手順
=======================================

1. OSをインストール
=======================================
ubuntu_server14.04-LTSをホストマシンにインストール


2. 各種設定
=======================================
1. パッケージリポジトリを日本ミラーサーバーに変更(jaist)
----------------------------------------------------
sudo sed -i'~' -E "s@http://(..\.)?(archive|security)\.ubuntu\.com/ubuntu@http://ftp.jaist.ac.jp/pub/Linux/ubuntu@g" /etc/apt/sources.list

2. システムのアップデート
----------------------------------------------------
sudo apt-get update
sudo apt-get upgrade

3. 必要なパッケージをインストール
----------------------------------------------------
sudo apt-get git vim ntp gcc


3. ネットワークの設定
=======================================
1. IPaddressの固定化
----------------------------------------------------
※詳しくはstudy/Linux/network/network.txtを参照

2. IPv6の無効化
----------------------------------------------------
sudo vim /etc/hosts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
127.0.0.1       localhost
127.0.1.1      stack01

# The following lines are desirable for IPv6 capable hosts
#::1     ip6-localhost ip6-loopback
#fe00::0 ip6-localnet
#ff00::0 ip6-mcastprefix
#ff02::1 ip6-allnodes
#ff02::2 ip6-allrouters

192.168.10.50   stack01
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

sudo vim /etc/sysctl.conf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
net.ipv6.conf.all.disable_ipv6 = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

3. 再起動
----------------------------------------------------
物理マシンから
virsh reboot [virtual_machine_name]


4. adduser stack (管理ユーザーでなければどのユーザーでも可)
=======================================
1. userを追加
----------------------------------------------------
sudo adduser stack
pass >> osozvi97
あとの質問 >> Enter

sudo userdel -r USER ※ミスったらこれで削除

2. 権限の変更
----------------------------------------------------
sudo visudo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
stack ALL=(ALL) NOPASSWD: ALL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

3. ユーザーの切り替え
----------------------------------------------------
su - stack


5. devstackのリポジトリをクローン
=======================================
git clone -b stable/kilo https://git.openstack.org/openstack-dev/devstack
git clone -b stable/liberty https://git.openstack.org/openstack-dev/devstack


6. local.confやlocal.shを編集
=======================================
cd devstack

vim local.conf >> local.confを編集
----------------------
1. Neutronの設定
----------------------
HOST_IP=192.168.0.101
>> ホストマシンのIPアドレスを指定

FLOATING_RANG=192.168.0.0/24
>> OpenStackの外部通信用アドレスとして用いるネットワークの範囲を指定
>> 基本的にはホストマシンのネットワークアドレスを指定

PUBLIC_NETWORK_GATEWAY=192.168.0.1
>> OpenStackの外部通信用アドレスのゲートウェイを設定
>> 基本的にはホストマシンが所属するネットワークのゲートウェイを指定

Q_FLOATING_ALLOCATION_POOL=start=192.168.0.200,end=192.168.0.220
>> インスタンスに割り振るFloatingIPの範囲を指定する。

FIXED_RANGE=192.168.14.0/24
>> OpenstackのVM間の通信で使用される、インターナルIPの範囲

NETWORK_GATEWAY=192.168.14.1
>> インターナルIPのデフォルトゲートウェイ


----------------------
2. Password and Token
----------------------
ADMIN_PASSWORD=admin
>> Openstackのユーザーは「ADMIN」と「非ADMIN」に分けられる。
>> ADMIN_PASSWORDでは「ADMIN」ユーザーのパスワードを設定する。

MYSQL_PASSWORD=sqlpass
>> Mysqlにログインするためのパスワード

RABBIT_PASSWORD=rabbitpass
>> Rabbitmqにログインするためのパスワード

SERVICE_PASSWORD=654bcdf38c6053c4a25d
>> 各サービスはKeystoneによる認証を利用するために、サービス自体をKeystoneに登録する
>> その際に用いるパスワードを指定

SERVICE_TOKEN
>> Keystoneを利用する際に使用

7. 起動と停止
=======================================
./stack.sh >> openstackを起動

This is your host ip: 192.168.0.101
Horizon is now available at http://192.168.0.101/
Keystone is serving at http://192.168.0.101:5000/
The default users are: admin and demo
The password: admin



./unstack.sh >> openstackを停止
