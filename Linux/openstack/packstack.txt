ホスト名の設定 root
==================--
hostnammectl set-hostname kvm01
echo "127.0.0.1 `hostname`" >> /etc/hosts

SELinuxの停止 root
====================
sed -i "s/SELINUX=*/SELINUX=disabled/" /etc/selinux/config
setenforce 0

NetworkManagerの停止 root
=======================
systemctl stop NetworkManager
systemctl disable NetworkManager
systemctl restart network
systemctl enable network


カーネルパラメータの設定 root
==============================
echo "net.ipv4.ip_forward=1" >> /usr/lib/sysctl.d/00-system.conf
echo "net.ipv4 conf.default.rp_filter=0" >> /usr/lib/sysctl.d/00-system.conf
echo "net.ipv4.conf.all.rp_filter" >> /usr/lib/sysctl.d/00-system.conf
echo "net.ipv4.conf.all.forwarding=1" >> /usr/lib/sysctl.d/00-system.conf
sysctl -e -p /usr/lib/sysctl.d/00-system.conf


パッケージのインストール root
==================================
★yum -y install https://repos.fedorapeople.org/repos/openstack/openstack-kilo/rdo-release-kilo.rpm
★yum -y install openstack-packstack epel-release

Packstack設定ファイルの生成
==================================
packstack --gen-answer-file answer.txt

sed -i 's/^CONFIG_PROVISION_DEMO=y/CONFIG_PROVISION_DEMO=n/' answer.txt
sed -i 's/^CONFIG_CEILOMETER_INSTALL=y/CONFIG_CEILOMETER_INSTALL=n' anser.txt


packstackの実行 root
=============================
packstack --answer-file answer.txt

packstack実行後の後処理 root
===========================
sed -i 's/^virt_type=*/virt_type=kvm/' /etc/nova/nova.conf

Proxy設定の削除 root
===========================
mv /etc/profile.d/proxy.sh /root/proxy_profile

ネットワークの設定 root
============================
★MACADDR=`ifconfi eth0 | grep ether | awk'{print $2}'``
IPADDR=`ifconfig eth0 | grep "inet" | awk '{print $2}'`
NETMASK=`ifconfig eth0 | grep "inet" | awk '{print $4}'`
GATEWAY=`route | grep "default" | awk '{print $2}'`

cat > /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DEVICE=eth0
ONBOOT=yes
MACADDR=$MACADDR
TYPE=OVSPort
DEVICETYPE=ovs
OVS_BRIDGE=br-ex
NM_CONTROLLED=no
EOF

cat > /etc/sysconfig/network-scripts/ifcfg-br-ex << EOF
DEVICE=br-ex
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSBridge
★OVSBOOTPORTO=none
★OVSDHCPINTERFACES=eth0
IPADDR=$IPADDR
NETMASK=$NETMASK
GATEWAY=$GATEWAY
OVS_BRIDGE=br-ex
NM_CONTROLLED=no
EOF


リブート root
===================
shutdown -r now


ネットワークの生成
======================
neutron router-create router1
TENANT_ID=`openstack project list | grep admin | awk '{print $2}'`
neutron net-create public --router:external --tenant-id $TENANT_ID
TENANT_ID=`openstack project list | grep service | awk '{print $2}'`
neutron net-create demo-net --shared --tenant-id $TENANT_ID
neutron subnet-create --name demo-net-subnet --enable_dhcp=True --allocation-pool=start=192.168.2.100,end=192.168.2.254 --dns-nameserver 192.168.2.201 demo-net 192.168.2.0/24
neutron router-gateway-set router1 public
neutron router-interface-add router1 subnet=demo-net-subnet
neutron subnet-list
neutron subnet-create --name public-subnet --enable_dhcp=False --allocation-pool=start=192.168.120.120,end=192.168.120.140 --gateway=192.168.120.1 public 192.168.120.0/24
