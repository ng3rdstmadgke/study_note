1. 公開鍵設定方法
====================

1.クライアント側の~/.ssh下でssh-keygenを実行し、公開鍵と秘密鍵のペアを作る。（最初の質問はブランクのままenter、2つ目の質問で秘密鍵のパスワードを入力）
2.クライアント側の公開鍵（id_rsa.pub）をホストの~/.ssh/authorized_keysに追記する(※authorized_keyがなければ、.ssh下に作成する)
3.ssh host_name@192.168.100.123コマンドで、sshに接続

git clone ssh://210.140.76.224//opt/gitrepo/thoth.git
git clone ssh://midorikawakeita@210.140.76.224//opt/gitrepo/thoth.git


2. ssh通常接続方法
========================
(サーバー側)
$ sudo apt-get install openssh-server
$ ps aux | grep sshd    # sshデーモンが動いているか確認

- ubuntu
sudo service sshd start
sudo service sshd stop
sudo service sshd restart

- mint
sudo service ssh start
sudo service ssh stop
sudo service ssh restart

(クライアント側)
$ sudo apt-get install openssh-client
$ ssh k-ta@192.168.0.8



3. X Window転送
===================
[サーバー側の設定]
- /etc/ssh/sshd_configに下記の設定があるかを確認
X11Forwarding yes      >> x11転送を許可
X11DisplayOffset 10    >> 擬似xサーバのディスプレイ番号
AddressFamily inet　　　>> IPv4のみで通信するように指定

- sudo vim /etc/sysctl.conf
net.ipv6.conf.all.disable_ipv6 = 1    >> IPv6を停止

- echo $DISPLAYが下記の出力をすることを確認
localhost:10.0

-xauth listが下記の出力をするか確認
remote-host/unix:10 MIT-MAGIC-COOKIE-1 88b30be2644732860782e9efbe029cbd

[クライアント側]
- sshで接続
ssh -X k-ta@192.168.0.123

- 転送したいWindowのコマンドを入力
例) chromium-browser


4. ssh-agentに秘密鍵を追加する
===================
ssh-add ~/.ssh/id_rsa
http://euske.github.io/openssh-jman/ssh-add.html
