<div>
    <h2>Index Page</h2>
    <?php if (isset($message)): ?>
    <?php foreach ($message as $key=>$value): ?>
    <div>
    <?php echo $key . ' : '; var_dump($value) ?><br>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>
    <hr>
    <div>
        <h3>Form Helper</h3>
        <?= $this->Form->create(null, ['type'=> 'post',
                                       'url' => ['controller' => 'Hello', 'action' => 'index']]) ?>
        <!--- テキストボックス --->
        <h5>テキストボックス</h5>
        <?= $this->Form->text('text', ['value' => 'こんにちは']) ?>

        <!--- パスワードフィールド --->
        <h5>パスワード</h5>
        <?= $this->Form->password('password', ['value' => 'osozvi97']) ?>

        <!--- 非表示フィールド --->
        <h5>非表示フィールド</h5>
        <?= $this->Form->hidden('hidden', ['value' => 'hidden message']) ?>

        <!--- テキストエリア --->
        <h5>テキストエリア</h5>
        <?= $this->Form->textarea('textarea', ['value' => 'ここはテキストエリアです']) ?>

        <!--- チェックボックス --->
        <h5>チェックボックス</h5>
        <?= $this->Form->checkbox('check1' ,['value' => 'spam', 'id' => '01', 'checked' => 'checked']) ?>
        <?= $this->Form->label('01', 'label01') ?>
        <?= $this->Form->checkbox('check2' ,['value' => 'ham', 'id' => '02']) ?>
        <?= $this->Form->label('02', 'label02') ?>
        <?= $this->Form->checkbox('check3' ,['value' => 'egg', 'id' => '03']) ?>
        <?= $this->Form->label('03', 'label03') ?>

        <!--- ラジオボタン --->
        <h5>ラジオボタン</h5>
        <?= $this->Form->radio('radio', [
            ['value' => 'Man', 'text' => 'male', 'checked' => 'checked'],
            ['value' => 'Woman', 'text' => 'female'],
            ['value' => 'Other', 'text' => 'other']
            ]) ?>

        <!--- セレクト1 --->
        <h5>セレクト1</h5>
        <?= $this->Form->select('select1', [
            ['value' => 'Mac', 'text' => 'Mac OS X'],
            ['value' => 'Windows', 'text' => 'Windows10', 'selected' => 'selected'],
            ['value' => 'Linux', 'text' => 'Ubuntu'],
            ['value' => 'Linux', 'text' => 'Centos'],
            ['value' => 'Linux', 'text' => 'RHEL'],
        ], ['style' => 'width:150px;']) ?>

        <!--- セレクト2 --->
        <h5>セレクト2</h5>
        <?= $this->Form->select('select2', [
            ['value' => 'Mac', 'text' => 'Mac OS X', 'selected' => 'selected'],
            ['value' => 'Windows', 'text' => 'Windows10', 'selected' => 'selected'],
            ['value' => 'Linux', 'text' => 'Linux'],
        ],['multiple' => true, 'size' => 3, 'style' => 'width:150px; height:200px;']) ?>

        <!--- 年月日 --->
        <h5>年月日</h5>
        <?= $this->Form->date('date', [
            'minYear' => 1990,
            'maxYear' => 2030,
            'year' => ['style' => 'width:100px; padding:5px;'],
            'month' => ['style' => 'width:100px;'],
            'day' => ['style' => 'width:100px;'],
        ]) ?>

        <!--- 時分 --->
        <h5>時分</h5>
        <?= $this->Form->time('time', [
            'interval' => 5,
            'hour' => ['style' => 'width:100px'],
            'minute' => ['style' => 'width:100px']
            ])?>

        <!--- 個別年月日時分 --->
        <h5>個別年月日時分</h5>
        <?= $this->Form->year('year', ['minYear' => 2016, 'maxYear' => 2030, 'style' => 'width:100px']) ?>
        <?= $this->Form->month('month', ['style' => 'width:100px']) ?>
        <?= $this->Form->day('day', ['style' => 'width:100px']) ?>
        <?= $this->Form->hour('hour', ['style' => 'width:100px']) ?>
        <?= $this->Form->minute('minute', ['interval' => 5, 'style' => 'width:100px']) ?>

        <?= $this->Form->submit('OK') ?>
        <?= $this->Form->end() ?>
    </div>
    <hr>
    <div>
        <h3>Normal Form</h3>
        <form method='post' action='/hello/index'>
            <!--- 年月日 --->
            <h5>年月日</h5>
            <input type='date' name='normal_date' style='width:250px;'>

            <!--- ファイル --->
            <h5>ファイル</h5>
            <input type='file' name='file' style='width:250px;'>

            <input type='submit' value='OK'>
        </form>
    </div>
</div>

