ミュータブル・イミュータブル
=================================================
ミュータブル : 値を変更することができるオブジェクト(データ型)
イミュータブル : 値を変更することができないオブジェクト(データ型)

ここで言う「値の変更」とは同一メモリ上に存在する値を変更できるかどうかということであり、
破壊的メソッド(戻り値を返すのではなく、自分自身の値を変更してしまうもの)を持っているオブジェクトが同一メモリ上の値を変更することができる。

■イミュータブル
str, int, tuple, bool
■ミュータブル
list, dict
※listはappendやconcat、dictはupdateやpopなどの破壊的メソッドを持っており、オブジェクト自身の値を変更することが可能である。


変数
=================================================
変数名はただの名前で、値を代入したからと言って値はコピーされない。
「変数名をつける=データを入れてあるオブジェクトに名前をつけているだけ」で、変数名は値自体ではなく値の参照である。

リテラル
=================================================
数値や文字列を直接記述した定数のこと。(変数の対義語)
変更されないことを前提とした値である。
リテラルを挿入する際は、型の判別のため特定の書式を用いる。
- 文字列リテラル : "hello", 'hello', """hello""", '''hello'''
- 数値リテラル : 10


数値
=================================================
イミュータブル。
python2ではintは32bit、lognは64bitだったが、python3ではそのような制限はなくなり、何桁でもintで表せるようになった。


基数
=================================================
int型はプレフィックスで特に基数を指定しない限り、10進数とみなされる
- なし : 10進数
- 0b : 2進数
- 0o : 8進数
- 0x : 16進数

[ex]
>>> 0x10
16


四則演算
=================================================
+ : 加算
- : 減算
* : 乗算
/ : 除算(float型で返還)
// : 除算(int型で返還　※小数点以下切り捨て)
% : 剰余
** : べき乗
+= : 加算代入
-= : 減算代入
*= : 乗算代入
/= : 除算代入


文字列
=================================================
イミュータブル
"spam", 'spam', """spam""", '''spam'''で表す。
"spam"と'spam'は単行のみ
"""spam"""と'''spam'''は複数行にわたって記述できる(改行部分は\nが入る)

[ex]
>>> """hello
... world"""
hello\nworld


行の継続
=================================================
pythonでは\を使うことで行を継続させることができる
[ex]
alphabet = "abcdefg" + \
        "hijklmnop" + \
        "qrstuvwxyz"


条件式でFalseとみなされるもの
=================================================
bool : False
null : None
int : 0
float : 0.0
str : ""
list : []
tuple : ()
dict : {}
set : set()

※上記以外は全てTrueとみなされる

while
=================================================
break : ループの中止
continue : 次のイテレーションの開始
else : ループがbreakせずに終了した時のみ実行されるブロック

for
=================================================
break : ループの中止
continue : 次のイテレーションの開始
else : ループがbreakせずに終了した時のみ実行されるブロック

ジェネレータ
=================================================
ジェネレータは一度だけしか実行できない。リスト、集合、文字列、辞書はメモリ内にあるが、
ジェネレータは一度に一つずつその場で値を作り、イテレータに足していってしまうので、作った値を覚えていない。
そのため、ジェネレータをもう一度つくったり、バックアップしたりすることはできない。


仮引数と実引数
=================================================
仮引数とは関数定義時に指定する引数のこと。
実引数とは関数呼び出し時に実際に使用する引数のこと。

関数の呼び出し
=================================================
pythonの括弧()は関数の呼び出しを意味する。
()がなければpythonは関数を他のオブジェクトと同様に扱う。
[ex]
>>> def echo(body):
...     print(body)
... 
>>> echo
<function echo at 0x7f37afc995f0>
>>> echo("hello")
hello


try, except
=================================================
try : エラーが起こると、exceptブロックが実行される
except : tryブロックで指定したエラーが起こると実行される
else : tryブロックでエラーが発生しなかった場合に実行される
finary : tryブロックでエラーが発生してもしなくても実行される


クラス定義の__init__
=================================================
__init__はクラス定義から個々のオブジェクトを作るときにそれを初期化するオブジェクト。
__init__があることによって、同一のクラス定義から、同時に複数の異なるオブジェクトを作成することが可能。
__init__で作られたオブジェクトを「インスタンス」と呼ぶ。

クラス定義のself引数
=================================================
self引数はつくられたオブジェクト(インスタンス)自体を参照することを示す。
同じクラスから作られた他のオブジェクトと、自身を区別するために必要な処理をするために使われる。

クラス継承用語
=================================================
■親クラス
スーパークラス
基底クラス

■子クラス
サブクラス
派生クラス

インスタンスメソッド・クラスメソッド・スタティックメソッド
=================================================
■インスタンスメソッド
メソッドの第一引数がselfとなっているメソッド。
インスタンスメソッドは、クラス定義から作られた個々のオブジェクト(インスタンス)をに対して処理を行うメソッドである。

■クラスメソッド
メソッドの第一引数がclsとなっており、@classmethodというデコレータがついたメソッド。
クラスメソッドは、クラス全体に影響を与える。

■スタティックメソッド
@staticmethiodというデコレータがついたメソッド。
スタティックメソッドはクラスにもオブジェクトにも影響を与えない。
厳密にクラスに含める必要も無いのだが、独立した存在としてふらふらしているよりも、都合がいいのでクラス定義の中にいるだけ。

ポリモーフィズム(多相性・多態性・多様性)
=================================================
オブジェクト指向プログラミング言語の持つ特性の一つ。
同名のメソッドをオブジェクトの種類に応じて使い分けることができる性質のこと。

例)
■モノモーフィズムでは何かの値を文字列に変換する際に、対象の型ごとに異なるメソッドを使用しなければならなかった
string = StringFromNumber(number)　# 数値 >> 文字列
string = StringFromDate(date)　# 日付 >> 文字列
■ポリモーフィズムな型システムを持つ言語では、型別にそれぞれ適切な変換方法を定義させることで、オブジェクトの種別によらない抽象度の高い変換方法を実現できる。
string = str(number)
string = str(date)

型付け
=================================================
■静的型付け
プログラム実行開始前に全ての変数の型を予め宣言しなければならない。
■動的型付け
プログラム実行中にその変数が呼び出される時まで、変数の方が変わりうる。

参考 : http://dic.nicovideo.jp/a/%E5%9E%8B%E4%BB%98%E3%81%91


クラス・モジュールを使うべきタイミング
=================================================
■クラス
- 動作(メソッド)は同じだが、内部状態(属性、アトリビュート)の異なる複数のインスタンスを必要とするとき
- 複数の値を持つ変数に複数の関数を使用する場合はクラスにまとめたほうが良い
■モジュール
ひとつの機能を複数回使用するとき。(モジュールはプログラムに何度参照されても1個のコピーしかロードされないため)

Unicode
=================================================
世界中の全ての数字・文字・記号を定義使用という発展途上の国際基準。
python3の文字列型はこのUnicodeで定義さえれる。
Unicodeは「\u」の後に4-8桁の16進数を続けて表現する


UTF8
=================================================

=================================================
=================================================
=================================================

=================================================
=================================================
=================================================
=================================================
=================================================

