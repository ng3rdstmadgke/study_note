クライアント・サーバーアプリケーション
	UIや結果を表示する部分と、データを受け取って処理を行う部分が分かれている。
	アプリケーションが動くときに通信を必要とする
	Webブラウザがリクエストを送り、リクエストを受け取ったWebサーバがレスポンスを返す。

スタンドアロン型アプリケーション
	UIと、操作に対応する処理を実行したり結果を表示するプログラムが一体化している。
	アプリケーションが動くときに通信を必要としない

リクエスト
	クライアント（ブラウザ）からサーバーに向かう通信。URLなどがある

	WebブラウザがＵＲＬの前半部分（スキームとホスト）を解釈し、
	リクエストを送信する方法と送信先がネットワークのどこにあるかを決める。
	ＵＲＬの後半部分（パスとクエリ）がwebサーバーにリクエストとして送られる。


	リクエストライン
		GET /cgi-bin/foo.py?bar=baz
		リクエストの種類とパスやクエリなどの情報が記載されている

	ヘッダフィールド
		HTTP/1.1 If-Modified-Since: Sun, 1 Jul 2007 11:31:06 GMT
		User-Agent: ....
		リクエスト送信時にwebブラウザがこっそりと埋め込んで送信する情報。
		表面には見えない
		ヘッダの情報は、環境変数としてｗｅｂサーバ上で動くプログラムに受け渡される。
		cgi.print_environ()で環境変数を表示すると、
		Webサーバを実行している環境に設定された情報が含まれています。
		CONTENT_TYPEやHTTP_USER_AGENTなどの環境変数の多くは、
		リクエストのヘッダで送信された情報です。

	ＨＴＴＰメソッド
		Webブラウザから送られるリクエストの種類GETメソッドとPOSTメソッドがある
		ＧＥＴメソッド
			Webサーバに対してレスポンスを要求するために、
			パスを指定したリクエストを発行するのに利用するのがGETメソッド
			HTMLファイルや画像ファイルのような静的ファイルを
			Webサーバに要求するときに主に利用します。
			Webサーバ上で動くプログラムに値を渡すときには、
			パスの後ろにクエリを追加する
			<form method="GET" action="///"></form>
			で指定のファイルにクエリを送信
		ＰＯＳＴメソッド
			Webサーバに対してデータを送信するときに利用するのがPOSTメソッド。
			<form method="POST" action="///"></form>
			で指定のファイルにクエリを送信
			cgi.FiedlStorage()関数では、
			POSTメソッドのレスポンスを解析して、クエリをオブジェクトに変換する。
			変換は自動的に行われるので、プログラム側でPOSTメソッドが使われているか
			GETメソッドが使われているかを意識する必要はあまり無い



レスポンス
	サーバーからクライアントへの通信
	HTMLや画像などのデータ。
	HTMLは文字列データ、画像などはバイナリデータと呼ばれている

	プログラムは標準出力にHTMLを出力します。
	するとWebサーバが通信を行い、Webブラウザにレスポンスとして返します。

	サーバから送り出されるレスポンスにはHTML以外にも多くのデータが含まれている。
	ステータスコード
		HTTP/1.1 200 OK

		200：正常に処理が完了
		400番代:クライアント側に原因のあるエラーが発生
		500番代:サーバー側に原因のあるエラーが発生
	レスポンスヘッダ
		Server: SimpleHTTP/0.6 Python/2.5
		Date: Sun, 1 Jul 2007 11:31:06 GMT
		Content-type: text/html
		Content-Length: 173

		空行を１行入れることで、レスポンスヘッダの終了を宣言する。

		Ｃｏｎｔｅｎｔ－ｔｙｐｅ（print文で書いてたおまじない）
			レスポンスがどのような種類のデータであるかを指定しています。
			text/htmlという部分がデータの種類を指示していて、HTML文書であることを示しています。
			Webブラウザは、このヘッダを解釈して、レスポンスの扱いを決めるのです。
			レスポンスヘッダとレスポンスボディの境目は空行１行
			image/jpegなどとすると、レスポンスのデータはJPEG画像として扱われます。





URL
	スキーム　　　ホスト　　　　パス　
	____   ______________ __________
	http://www.python.org/about/apps/

	スキーム：どのような手法を使ってリクエストを送るかを指定する
	ホスト：リクエストを送る先がインターネット上のどこにあるかを指定する
	パス：ファイルが存在する場所


	スキーム　 ホスト　ポート　　  パス　       クエリ
	____   _________ ____ _______________ _______
	http://127.0.0.1:8000/cgi-bin/test.py?foo=bar

	URLは前半（スキームとホスト）と後半（パスとクエリ）に別れる
	Webブラウザがスキームとホストを解釈し、
	リクエストを送信する方法と送信先がネットワークのどこにあるかを決めます。




webサーバー
	リクエスト（URLのパスなど）を解釈しHTMLを応答として返す一種のプログラム

静的出力
	事前に作っておいたファイルを使ってリクエストにレスポンスすること

動的出力
	プログラムを使って組み立てたレスポンスを返すこと

127.0.0.1
	ループバックアドレス、webブラウザと同じマシンを指している

http.serverモジュール
	python3 -m http.server 8000
	サーバーを起動させることができる。
	カレントディレクトリにあるファイルを見せるような動きをする
	http://127.0.0.1:8000/をブラウザに入力するとブラウザに表示される

ドキュメントルート
	webサーバがファイルを扱う起点となるディレクトリのこと

	ドキュメントルートに相当する場所にindexhtmlという名前のHTMLファイルを置くと、
	webブラウザにはそのファイルが表示される。

ハンドラ
	何かの出来事が起きた時にだけ、決まった処理をするもののこと
	リクエストハンドラー = リクエストを受け取るためにずっと待機しているプログラム（デーモン）

	http.server.SimpleHTTPRequestHandler
		リクエストを受け取るという出来事が起きると、
		起動した時のカレントディレクトリにあるファイルを見せる

	http.server.CGIHTTPRequestHandler
		リクエストを受け取るという出来事が起きると、
		サーバ上のPythonのプログラムを動かす
		※レスポンスはwebブラウザが受け取って表示する。
		※Pythonプログラム（サーバ）とWebブラウザ（クライアント）の間で通信ができるようになる

クエリ（Query）
	「問い合わせ」、「質問」の意味。
	リクエストに添える問い合わせという意味で捉えておくと良い。
	クエリはURLのパスの後ろに?をつけて記述する。
	「http://127.0.0.1:8000/cgi-bin/test.py?foobarbaz」

	クエリを使うと、Webサーバ上のプログラムに値を渡すことができる
	クエリには、キーと値のペアをイコール(=)で挟んだ形でデータを記述できる
	複数の値を渡したいときには、キーと値のペアを「アンド(&)」で区切る

	サーバー上のプログラムはクエリを受け取ることで、
	クライアントからの指示を受け取ることができる

	クエリの文字列は(英数字)、(-)、(_)、(.)以外は、
	%+16進数の文字コードにエスケープされる

	フォームからGETメソッドでリクエストを送信する場合には、
	Webブラウザが自動的にURLエンコードを行います。
	プログラム側では、クエリの文字列を解析してキーと値に分割して処理をします。
	クエリの値はURLエンコードされているので、エンコードを解いて、
	元の文字列を取り出す処理をします。
	FieldStorage()関数はクエリの解析を自動的に行うため、
	ＵＲＬエンコードを自分でやることはほぼ無い



cgiモジュール
	URL上にクエリとして渡された値を受け取るためのモジュール。
	cgi.print_environ()
		環境変数を表示する
	form=cgi.FieldStorage()
		クエリとして渡された値をPythonのプログラムで扱えるように変換できる
		リクエストの内容をｐｙｔｈｏｎの文字列などのデータ型に変換する
		keyとvalueでdictのようなオブジェクトを生成
	form.keys()
		存在するキー一覧を返す
	"name"　in form
		nameというキーが存在しているかをＴｒｕｅ、Ｆａｌｓｅで返す
	form['foo'].value
		fooというキーを持つクエリに対応する値を取得する。
		http://127.0.0.1:8000/cgi-bin/querytest.py?foo=bar
		上の例だと"bar"という文字列を返す
		キーが存在しない場合はエラーを返す
	form.getvalue('foo', 'N/A')
		fooというキーを持つクエリに対応する値を取得する。
		fooというキーを持つクエリがないときでも、エラーを返さず、
		第二引数に取った文字列を返す
	form.getlist("name")
		nameというキーを持つクエリに対応する値をリストで取得する
	form.getfirst("name","default")
		nameというキーを持つクエリに対応する値を取得する
		nameの値が１つの場合も複数の場合も必ず最初の要素となる文字列を

<form>タグ
　　　　<form action="/cgi-bin/find13f.py" method="GET">
　　　　西暦を入力してください :<input type="text" name="year" />
　　　　　　　　<input type="submit" name="submit" />
　　　　</form>

	<form>
		action:リクエストを送る先を指定する
		ｍｅｔｈｏｄ：クエリの送信方法を指定する
			ＧＥＴ：クエリをＵＲＬに埋め込んで送信する。
			GETメソッドは、特定の性質を持ったデータを取得したいときに利用されます。
			GETメソッドは、Webアプリケーションに送信するデータがURLとして残ります。
			このため、リンクにクエリを含んだURLを埋め込む、というような使い方ができる。
			何度も同じ内容のクエリを送信できるのです。
			半面、クエリの長さに制限があります。
			そのため、大きなサイズのデータを送ることができません。

			ＰＯＳＴ：ＵＲＬに埋め込まずにクエリを送信できる
			POSTメソッドは、データの新規作成や更新のために利用されます。
			データのサイズにも制限がないため、大きなデータを送信できます。
			ただし、クエリのデータはリクエストの背後で送信されます。
			同じ内容のクエリを再送信するには、
			フォームなどから同じデータを再度POSTする必要があります。

	テキストフィールド
		<input type="text" name="first_name" size="20" />
		type:入力の形式
		name:クエリのキー
		value:フォームの初期値を設定できる。

	サブミットボタン
		<input type="submit" name="submit" value="送信" />
		同じフォームに複数のサブミットボタンを設置し、name、valueを変えることで、
		Webアプリケーション側で押されたボタンを判別できるようになります。

	リセットボタン
		<input type="reset" name="reset" value="クリア" />
		リセットボタンは、フォームの入力値をクリアするためのボタンです。
		valueアトリビュートに指定した文字列がボタンのラベルとして表示されます。

	ラジオボタン
		<input type="radio" name="gender" value="male">男性
		<input type="radio" name="gender" value="female">女性
		同じnameを持つラジオボタンのタグがグループとして扱われます。
		タグにcheckedというアトリビュートを「checked="checked"」のように設置すると、
		そのコントロールがあらかじめ選択された状態で表示されます。

	チェックボックス
		<input type="checkbox" name="language" value="Python">Python
		<input type="checkbox" name="language" value="Ruby">Ruby
		コントロールをあらかじめ選択した状態で表示したいときには、
		「checked="checked"」というアトリビュートをタグに設置します。
		チェックボックスから受け取った値はlistの型をとる

	メニュー
		<select name="language">
		  <option value="python">Python</option>
		  <option value="ruby">Ruby</option>
		  <option value="perl">Perl</option>
		  <option value="php">PHP</option>
		</select>
		selectedというアトリビュートを<option>に設置すると、
		その項目が選択された状態でコントロールが表示されます。

	テキストエリア
		<textarea name="body_text" cols="40" rows="20" >
		文章を入力してください
		</textarea>
		複数行にわたる、テキスト入力用のコントロール。

フォームから送信される文字のエンコード
	特殊なケースを除いて、フォームから送信される文字列のエンコードは、
	フォームを表示しているWebページ（HTML）のエンコードと同じと思ってほぼＯＫ


コントロール
	テキストフィールドや、ボタンなど、<form>を用いたＵＩの部品

クエリのvalueがリストで帰ってくる場合
	チェックボックスで複数の項目を選択した場合。
	同じフォームの中に同一のnameを持つコントロールが複数あり、
	それぞれの項目に値が設定されている場合。

HTTP(HyperText Transfer Protocol)
	前半のHTはHTMLの前半と同じで、
	他の文書へのリンクを埋め込める仕組みのことを指しています。
	後半のTPは通信のためのプロトコルというような意味です。

パーサー
	プログラムのソースコードやXML文書など、
	一定の文法に従って記述された複雑な構造のテキスト文書を解析し、
	プログラムで扱えるようなデータ構造の集合体に変換するプログラムのこと。
	そのような処理のことを「構文解析」「パース」（parse）という。

RSS
	サイトの情報を配信するための規格のひとつ。
	これを配信することにより、ユーザー側の情報取得・整理が容易になる。
	RSS 2.0 / Really Simple Syndication
	コンテンツおよびコンテンツ配信を主眼に置いたフォーマット。
	XMLで記述され、海外で広く使われている。

RSSリーダー
	ブログやポッドキャスト、ニュースサイト等が配信するRSSを読むためのツールの総称。
	更新チェックと概要確認が同時にできる点が利点。
	気に入ったWebサイトのフィード（RSSまたはAtom）のURLを登録すると、
	そのWebサイトにアクセスしなくても最新情報の概要を確認できる。
	その概要を見て、さらに詳しく読みたい場合は本Webサイトを開く。

ヒアドキュメント
	HTMLのような文字列とプログラムが混在したプログラム

セッション
	Webブラウザがリクエストを送り、
	リクエストを受け取ったWebサーバがレスポンスを返す。
	という一連の流れ。
	ｗｅｂアプリケーションの処理は、セッションを１つの単位として進んでいく。

データ
	基本的にｗｅｂのデータ通信の仕組みには、データを保存するための方法がない。
	そのため、リクエストとして送ったデータや、
	ｗｅｂアプリケーション内部で作成したデータはプログラムが終了するとともに消えてしまう。

xml.etree.ElementTree
	#文字列から作成
	elem_f = fromstring(xml_str) #文字列からルートエレメントを取得(Element型)

	#ファイルから作成
	atree = parse("sample.xml") #返り値はElementTree型
	tree = ElementTree(file="sample.xml")#ElementTree.parse(XMLFILE)でもＯＫ
	elem_ff = tree.getroot() #ルートエレメント（ノード）を取得(Element型)


	"""データの参照"""
		#要素のタグを取得
		print(elem_f.tag)
		#textを取得
		#attributeをdict型で取得
		print(elem_f.attrib)
		#attribute名のリスト取得
		print(elem_f.keys())
		#(attribute, value)形式タプルのリスト取得
		print(elem_f.items())

		#attribute名を指定して値を取得
		print(elem_f.get("std"))
		#デフォルトを指定してattributeを取得
		print(elem_f.get("height","1200"))


	"""要素の検索"""

		# 子要素の中から特定の子だけを取り出す。
		#（find()は上から走査して、最初に見つかった要素1つだけを返す）
		a = elem_f.find("male")
		print("tag:{} text:{} att:{}".format(a.tag,a.text,a.attrib))

		#子要素からマッチするエレメントを持つすべての子を取り出す
		n = elem_f.findall("male")
		for a in n:
		    print("tag:{} text:{} att:{}".format(a.tag,a.text,a.attrib))

		# すべての子・孫要素の中からマッチするエレメントを探す
		g = elem_f.findall(".//name")
		for a in g:
		    print("tag:{} text:{} att:{}".format(a.tag,a.text,a.attrib))


	"""要素への順次アクセス"""
		print("\n要素への順次アクセス")

		#すべての要素にアクセス
		for e in elem_f.getiterator():
		    print(e.tag)

		#条件指定（tag名を指定）
		for e in elem_f.getiterator("age"):
		    print(e.tag)

		#子要素そのリストを取得(再帰的ではなく、直系の子要素のみ)
		for e in list(elem_f):
		    print(e.tag)

シリアライズ
構造を持ったデータをファイルに書き出すためには、
一度前から後ろに読めるように変換を行う必要がある。
このような処理のことをシリアライズ
pickleモジュールを使う

pickleモジュール
	dumps(obj)
		シリアライズした結果を文字列として返す
	dump(obj,file)
		ファイルオブジェクトを引数に添え、シリアライズした結果をファイルに書き出す。
		f = open("filename.txt", "wb")
		pickle.load(f)
	loads(string)
		dumps(),dump()でシリアライズした文字列からpythonオブジェクトを復元する関数。
	load(file)
		シリアライズした結果を書きだしたファイルを引数にとり、
		ｐｙｔｈｏｎオブジェクトを復元する。
		f = open("filename.txt", "rb")
		pickle.load(f)

	>>> import pickle
	>>> l=[1, 2, "three", "four"]
	>>> ps=pickle.dumps(l)
	>>> ps
	b'\x80\x03]q\x00(K\x01K\x02X\x04\x00\x00\x00treeq\x01X\x04\x00\x00\x00fourq\x02e.'
	>>> pl=pickle.loads(ps)
	>>> pl
	[1, 2, 'three', 'four']

マルチスレッド
	多くのリクエストを同時に処理するプログラム
	pickleモジュールなどはマルチスレッドだと、データの書き出しがうまくできない。

リレーショナル・データベース
	excelみたいな形式のデータベース

SQL
	データをデータベースに保存したり、データを取り出したりするための問い合わせ言語
	column→列（列の名前を「カラム名」という）
	row→行

	INSERT文（データの登録）
		シリアル型のidのようなカラムは、
		データが自動的に挿入されるので指定する必要はない）
		INSERT INTO address (postcode, prefecture, street)　VALUES("100-0000", "東京都", "千代田区1-1");

	SELECT文（データの選択）
	　WHERE句ではデータを取り出す条件を指定
		SELECT prefecture, street FROM address　WHERE postcode="100-0000";

	UPDATE文（データの更新）
		UPDATE address SET postcode="001-0000", prefecture="北海道" WHERE id=10;

	DELETE文（データの削除）
		DELETE FROM address WHERE id=11;
----------------------------------------------------------------
#!/usr/bin/env python3
import sqlite3
con = sqlite3.connect("test.sqlite")
cur = con.cursor()
#c
try:
    cur.execute("""CREATE TABLE address(
                id serial, postcode text,
                 prefecture text, street test);""")
except:
    pass

#insert
for i in range(1,20):
    cur.execute("""INSERT INTO address(postcode, prefecture, street)
    VALUES('{}', 'Tokyo', 'Chiyodaku');""".format(i))

#select fetchone fetchall
cur.execute("""SELECT postcode, prefecture, street FROM address WHERE prefecture="Tokyo";""")
for i in range(10):
    print(cur.fetchone())
print(cur.fetchall())

#update
cur.execute("""UPDATE address SET street="nisitoukyou" WHERE postcode=1;""")
cur.execute("""SELECT postcode, prefecture, street FROM address WHERE prefecture="Tokyo";""")
print(cur.fetchall())

#delete
cur.execute("""DELETE FROM address WHERE street="Chiyodaku";""")
cur.execute("""SELECT postcode, prefecture, street FROM address WHERE prefecture="Tokyo";""")
print(cur.fetchall())

----------------------------------------------------------------


テーブル
	保存するデータの形式に合わせ、予め表の形式を定義しておくこと、
	もしくは、その表の形式
	※テーブル定義（カラム名とデータ型を指定する。）
	addressはテーブル名
	CREATE TABLE address (postcode text,prefecture text,address text );

シリアル型
	テーブルを定義する際のデータ型
	新しい列が追加されると、番号が一つづつ増えていく特殊なデータ型
	CREATE TABLE address (id serial,postcode text);


データベースとのデータのやり取り
	+-------+				+----------+	+-------+
	|		|→ SQLを送信 →   |  		   | →　|		|
	|python |				|　　DB接続  |		|   DB  |
	|		|←結果を受信 ←   | モジュール|	　←　|		|
	+-------+				+----------+	+-------+


DBAPI
	Pythonには、いろいろなデータベースに接続するためのモジュールが用意されています。
	しかし、接続や通信の方法はモジュールごとにまちまちであることがほとんどです。
	通信や接続の方法をある程度統一し、Pythonからさまざまなデータベースを
	より手軽に利用できるようにする目的で作られたＡＰＩ
	sqlite3などがある

sqlite3モジュール
コネクションオブジェクト
	データベースとの接続を行うときに利用するオブジェクト。
	dbnameにはファイルをフルパスか相対パスで指定する。
	データベースの操作が終わり、接続を切るためにもコネクションオブジェクトを使います。
	con = sqlite3.connect("dbname")

カーソル
	cursor()
	カーソルオブジェクトを使いコネクションオブジェクトに指定されたＤＢに対して通信を行う
	execute()メソッドでSQLを発行して、
	必要に応じてfetchone()やfetchall()を使ってデータを取り出す、
	という処理の流れになります。
	cur = con.cursor()

	exec = cur.execute(sq(lSQL文字列)[,パラメータ])
		SQLを引数として渡し、実行します。
		execute()メソッドを実行しただけでは、
		insert文やupdate文によるデータベースの更新は行われない
		データベースに対する更新を有効にするためには、
		コネクションオブジェクトのcommit()というメソッドを呼ぶ必要がある。
		commit()メソッドを呼ぶと,トランザクションが閉じられます。
		commit()メソッドを呼ぶ前にカーソルオブジェクトが消滅すると、
		トランザクションがロールバックされてしまい,更新の内容がキャンセルされる。
		明示的にトランザクションをロールバックしたいときは,rollback()メソッドを呼ぶ。

	exec.fetchone()
		カーソルから選択したデータを1行だけ取り出します。
		戻り値は、選択した結果を順番に格納したタプルとなります。
		データがない場合にはNoneが返ってきます。
		SQLを使ってデータベースからデータを選択した後で呼び出します。

	exec.fetchall()
		カーソルから選択したデータをすべて取り出します。
		戻り値は、選択した結果を順番に格納したタプルのタプルとなります。
		また、選択を行った後のカーソルオブジェクトをfor文に添えると、
		このメソッドが呼び出されます。
		選択結果を1行づつ繰り返し変数に代入して処理を行えるわけです。

	cur.close()
		カーソルを閉じます。
		close()メソッドを呼んだ後のカーソルで
		execute()メソッドなどを呼び出そうとするとエラーになります。


プレースホルダ
	置換用の文字列を含んだＳＱＬのテンプレート。execute()で利用できる。
	プレースホルダは、文字列をクエリとして記述するときに
	必要なクオーテーションやエスケープ処理などを自動的に行います。
	WebアプリケーションなどでパラメータにSQLを挿入することで起こる
	SQLインジェクションというセキュリティ上の問題も起こりにくくなります。
	文字列を連結してSQLを作るより、手軽で安全な手法と言えます。

	1.クエスチョンマークを使う方法
	オプションの引数にタプルを渡すと、クエスチョンマークのある場所に
	オブジェクトが順番に埋め込まれます。
	>>>name = "guido"
	>>>age = 23
	>>>cur.execute("""SELECT lastname FROM people　
					WHERE firstname=? AND age=?""",(name,age))


	2.辞書のキーを使って置換する場所を指定する方法
	クエスチョンマークを使った方法と違って、
	引数の順番の影響を受けないのが便利な点です。
	>>>name = "guido"
	>>>age = 23
	>>>cur.execute("""SELECT lastname FROM people
               WHERE firstname=:name AND age=:age""",
               {'name':name,'age':age})


テンプレートエンジン
	プログラムとは別のファイルにＨＴＭＬ文字列を書いておき、必要に応じて読み込んで使うための仕組み。
	プログラムが記述してあるファイルにＨＴＭＬがあると可読性を損なうため別ファイルとして用意する。

	Pythonで利用できるテンプレートエンジンの多くは
	クラスとして実装されています。
	まず、テンプレート文字列を指定するなどして
	クラスのインスタンスオブジェクトを作ります。
	その後、テンプレートに埋め込まれたパターンを置換するための
	メソッドを呼び出して、Webアプリケーションの出力となるHTMLのような
	文字列を作ります。

ＭＶＣモデル
	Ｍ→モデル：データベースに関わる処理を行う部分
	Ｖ→ビュー：表示に関わる部分
	Ｃ→コントローラ：リクエストを受け取ったりデータを加工したりする部分

正規表現
	正規表現で使用する特殊文字（メタ文字） → . ^ $ [ ] * + ? | ( )
	メタ文字を文字列中で使用したい場合は\を直前につける。例）\$ \.

	とにかく何でもいい一文字 → .
		私は.です
		→私は鳥です　私は0です

	行の先頭に存在する文字列を検索 → ^
		^ありがとう
		→ありがとうと言いたい　ありがとうございました

	行の最後に存在する文字列を検索 → $
		ありがとう$
		→今日はありがとう　君に心よりありがとう

	直前の文字列が0個以上連続する → *
		おー*い
		→おい　おーい　おーーーーーい

	直前の文字が1個以上連続する → +
		おー+い
		→おーい　おーーい　おーーーーーーーい

	直前の文字が0個あるいは1個だけある → ?
		Windows?
		→Windows　Window

	いずれかの文字列 → |
		IBM|マイクロソフト|Apple|ネットスケープ
		→IBM　マイクロソフト Apple　ネットスケープ

	指定した文字列のどれか → []
		※[]の中では、メタ文字は普通の文字として認識される
		明日は[晴曇雨]です
		→明日は晴です　明日は曇です　明日は雨です

		A[A-Z]CCC
		→ABCCC AKCCC AZCCC AGCCC

		A[0-9]CCC
		→A3CCC A9CCC A0CCC

	文字をグループ化
		(じゃ)+ーん
		→じゃーん　じゃじゃーん　じゃじゃじゃーん






	応用
	何でもいい文字の連続 → .*
		「君が好き.*。」
		→君が好きです。　君が好きかもね。　君が好きだちゅうの。


ｗｅｂサーバー
基本的にはリクエストを受けてレスポンスを返すという単純なもの
CGIHTTPServerは２つのクラスから作られている
１．HTTPServerクラス
ネットワークの通信を担当（BaseHTTPServerというモジュールに定義されている）

2.CGIHTTPRequestHandlerクラス
大まかにリクエストを受け取り実際に処理を行うためのクラスである。
CGIHTTPRequestHandlerにはＣＧＩを実行するために必要な部分のみが記載されている。
このクラスはSimpleHTTPRequestHandlerというクラスを継承していて、
静的なファイルを配信する一般的なWebサーバに求められる機能はSimpleHTTPRequestHandlerクラスに定義されています。
更にSimpleHTTPRequestHandlerはBaseHTTPRequestHandlerという抽象クラスを継承しています。
